<?php

error_reporting( E_ALL);
ini_set('display_errors', 'On');

spl_autoload_register(function ($class) {
    $filename = __DIR__ . '/../lib/' . str_replace('\\', DIRECTORY_SEPARATOR, $class) . ".php";
    if (file_exists($filename)) {
        include $filename;
    }
});

\MovieRec\SessionContext::create();


require_once __DIR__ . '/../lib/Data/DataManager_PDO.php';