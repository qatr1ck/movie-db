<?php

require_once 'inc/bootstrap.php';

use MovieRec\Controller;


$defaultView = 'welcome';
$view = $defaultView;


$postAction = isset($_REQUEST[Controller::ACTION]) ? $_REQUEST[Controller::ACTION] : null;
if (!is_null($postAction)) {
    Controller::getInstance()->invokePostAction();
}


if(isset($_REQUEST['view']) && file_exists(__DIR__ . '/views/' . $_REQUEST['view'] . '.php')) {
    $view = $_REQUEST['view'];
}

require_once 'views/' . $view . '.php';