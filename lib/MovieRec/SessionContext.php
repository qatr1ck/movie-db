<?php


namespace MovieRec;


class SessionContext
{

    private static $exists = false;

    /**
     * @return bool
     */
    public static function create(): bool {
        if (!self::$exists) {
            self::$exists = session_start();
        }
        return self::$exists;
    }
}