<?php
namespace MovieRec;

interface IData {
    public function getId(): int;
}

class Entity extends BaseObject implements IData {

    private $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Entity constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }
}