<?php


namespace MovieRec;

class User
{
    private $id;
    private $username;
    private $password;
    private $graphdbId;

    /**
     * User constructor.
     * @param $id
     * @param $username
     * @param $password
     */
    public function __construct($id, $username, $password, $graphdbID)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->graphdbId = $graphdbID;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }


    /**
     * @return mixed
     */
    public function getGraphdbId()
    {
        return $this->graphdbId;
    }



}