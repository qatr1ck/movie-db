<?php


namespace MovieRec;

use DataManager;

class AuthenticationManager extends BaseObject
{

    /**
     * @param string $userName
     * @param string $password
     * @return bool
     */
    public static function authenticate(string $userName, string $password): bool {
        $user = DataManager::getUserByUserName($userName);

        if (!is_null($user) && $user->getPassword() === hash(
            'sha1', "$userName|$password"
            )) {
            $_SESSION['user'] = $user->getId();
            $_SESSION['userGraphId'] = $user->getGraphdbId();
            return true;
        }
        self::signOut();
        return false;
    }

    /**
     *
     */
    public static function signOut() {
        unset($_SESSION['user']);
    }

    /**
     * @return bool
     */
    public static function isAuthenticated(): bool {
        return isset($_SESSION['user']);
    }

    /**
     * @return User|null
     */
    public static function getAuthenticatedUser() {
        return self::isAuthenticated() ? DataManager::getUserById($_SESSION['user']) : null;
    }

    /**
     * @param string $userName
     * @return bool
     */
    public static function userExist(string $userName) : bool {
        return !is_null(DataManager::getUserByUserName($userName));
    }


}