<?php


namespace MovieRec;


class Controller
{
    private static $instance = false;

    const ACTION = 'action';
    const ACTION_SEARCH = 'search';
    const ACTION_SIM = 'similar';
    const ACTION_LOGIN = 'login';
    const ACTION_LOGOUT = 'logout';
    const ACTION_REGISTER = 'register';
    const ACTION_ADD_WATCHED = 'addWatched';
    const ACTION_REMOVE_WATCHED = 'removeWatched';

    const MOVIE_NAME = 'movieName';
    const USER_NAME = 'userName';
    const USER_PASSWORD = 'password';
    const MOVIE_ID = 'movieId';

    const PAGE = 'page';

    /**
     * Controller constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return Controller
     */
    public static function getInstance(): Controller
    {
        if (!self::$instance) {
            self::$instance = new Controller();
        }
        return self::$instance;
    }

    /**
     * @throws \Exception
     */
    public function invokePostAction()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new \Exception('Controller can only handle POST requests');
        } elseif (!isset($_REQUEST[self::ACTION])) {
            throw new \Exception(self::ACTION . ' not specified');
        }

        $action = $_REQUEST[self::ACTION];
        switch ($action) {
            case self::ACTION_LOGIN:
                if (!AuthenticationManager::authenticate(
                    $_REQUEST[self::USER_NAME], $_REQUEST[self::USER_PASSWORD]
                )) {
                    $this->forwardRequest(array('Invalid username or password'));
                }
                UserRecommendations::update($_SESSION['userGraphId']);
                Util::redirect("index.php?view=welcome");
                break;
            case self::ACTION_LOGOUT:
                AuthenticationManager::signOut();
                Util::redirect("index.php?view=welcome");
                break;
            case self::ACTION_REGISTER:
                if(AuthenticationManager::userExist($_REQUEST[self::USER_NAME])) {
                    $this->forwardRequest(array('Invalid username'));
                } else {
                    \DataManager::insertUser($_REQUEST[self::USER_NAME], $_REQUEST[self::USER_PASSWORD]);
                }
                Util::redirect("index.php?view=login");
                break;
            case self::ACTION_SEARCH:
                $_SESSION['search'] = $_POST[self::MOVIE_NAME];
                Util::redirect("index.php?view=moviesearch");
                break;
            case self::ACTION_SIM:
                SimMovies::clear();
                SimMovies::setShown(True);
                SimMovies::update($_REQUEST[self::MOVIE_ID]);
                Util::redirect();
                break;
            case self::ACTION_ADD_WATCHED:
                \DataManager::addWatched($_SESSION['userGraphId'], $_REQUEST[self::MOVIE_ID]);
                UserRecommendations::update($_SESSION['userGraphId']); // Update recommendations for user
                Util::redirect();
                break;
            case self::ACTION_REMOVE_WATCHED:
                \DataManager::removeWatched($_SESSION['userGraphId'], $_REQUEST[self::MOVIE_ID]);
                UserRecommendations::update($_SESSION['userGraphId']); // Update recommendations for user
                Util::redirect();
                break;
            default:
                throw new \Exception("Unknown Controller Action $action");
        }
    }

    /**
     * Forwarding a request with an error. Only works if we define a target where
     * we want to redirect to, otherwise we try to infer it from the page parameter;
     * Appending the errors works by serialising an array to create a string and then
     * encode it properly for the URL
     *
     * @param array|null $errors
     * @param null $target
     * @throws \Exception
     */
    private function forwardRequest(array $errors = null, $target = null)
    {
        if (is_null($target)) {
            if (!isset($_REQUEST[self::PAGE])) {
                throw new \Exception('Missing target for forward');
            } else {
                $target = $_REQUEST[self::PAGE];
            }
        }

        if (count($errors) > 0) {
            $target .= '&errors=' . urlencode(serialize($errors));
        }
        header("Location: $target");
        exit();
    }


}