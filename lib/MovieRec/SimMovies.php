<?php

namespace MovieRec;

use DataManager;

class SimMovies extends BaseObject {

    /**
     * @return array
     */
    private static function getSimMovies() : array {
        return isset($_SESSION['simmovies']) ? $_SESSION['simmovies'] : array();
    }

    /**
     * @param array $simmovies
     */
    private static function storeSimMovies(array $simmovies) {
        $_SESSION['simmovies'] = $simmovies;
    }

    /**
     * @param int $movieId
     */
    public static function update(int $movieId) {
        self::clear();
        $_SESSION['simto'] = DataManager::getMovieById($movieId);
        $simmovies = DataManager::getSimilarMovies($movieId);
        self::storeSimMovies($simmovies);
    }

    /**
     * @return int
     */
    public static function size() : int {
        return count(self::getSimMovies());
    }

    /**
     *
     */
    public static function clear() {
        self::storeSimMovies(array());
    }

    /**
     * @return array
     */
    public static function getAll() : array {
        return self::getSimMovies();
    }

    /**
     * @return bool
     */
    public static function isShown(): bool
    {
        if (isset($_SESSION['showSimmovies'])) {
            return $_SESSION['showSimmovies'];
        } else {
            return false;
        }
    }

    /**
     * @param bool $shown
     */
    public static function setShown(bool $shown): void
    {
        $_SESSION['showSimmovies'] = $shown;
    }

}