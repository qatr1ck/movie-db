<?php

namespace MovieRec;

use MovieRec\BaseObject;
use DataManager;

class UserRecommendations extends BaseObject {

    /**
     * @return array
     */
    private static function getMovieRecommendations() : array {
        return isset($_SESSION['recommendations']) ? $_SESSION['recommendations'] : array();
    }

    /**
     * @param array $recommendations
     */
    private static function storeRecommendations(array $recommendations) {
        $_SESSION['recommendations'] = $recommendations;
    }

    /**
     * @return int
     */
    public static function size() : int {
        return count(self::getMovieRecommendations());
    }

    /**
     *
     */
    public static function clear() {
        self::storeRecommendations(array());
    }

    /**
     * @param int $userId
     */
    public static function update(int $userId) {
        self::clear();
        self::storeRecommendations(DataManager::getRecommendationForUser($userId));
    }

    /**
     * @return array
     */
    public static function getAll() : array {
        return self::getMovieRecommendations();
    }

}
