<?php


namespace MovieRec;


class Util extends BaseObject
{

    /**
     * escapes the output
     *
     * @param string $string target string
     * @return string
     */
    public static function escape(string $string): string {
        // \r\n -> <br>
        return nl2br(htmlentities($string));
    }

    /**
     * Redirect to a specific page if defined, otherwise redirect to where we came from
     *
     * @param string|null $page
     */
    public static function redirect(string $page = null) {
        if (is_null($page)) {
            $page = isset($_REQUEST[Controller::PAGE]) ?
                $_REQUEST[Controller::PAGE] :
                $_SERVER['REQUEST_URI'];
        }
        header("Location: $page");
        // exit because otherwise script would continue to execute
        exit();
    }

    /**
     * GET parameter "page" adds current page to action so that a redirect
     * back to this page is possible after successful execution of POST action
     * if "page" has been set before then just keep the current value (to avoid
     * problem with "growing URLs" when a POST form is rendered "a second time"
     * e.g. during a forward after an unsuccessful POS action)
     *
     * Be sure to check for invalid / insecure page redirects!!
     *
     * @param string $action  uri optional
     * @param array $params  array key/value pairs
     * @return string
     */
    public static function action(string $action, array $params = null) : string {
        $page = isset($_REQUEST[Controller::PAGE]) ?
            $_REQUEST[Controller::PAGE] :
            $_SERVER['REQUEST_URI'];

        $res = 'index.php?' . Controller::ACTION . '=' . rawurlencode($action)
            . '&' . Controller::PAGE . '=' . rawurlencode($page);

        if (!is_null($params)) {
            foreach ($params as $name => $value) {
                $res .= '&' . rawurlencode($name) . '=' . rawurlencode($value);
            }
        }
        return $res;
    }


    /**
     * Convert a rating score to persenctage of total (max 5)
     *
     * @param $rating
     * @return float
     */
    public static function getPercentageRating($rating) : float {
        if ($rating == null) {
            return 0.0;
        }
        else {
            return round($rating, 1)/5 * 100;
        }
    }


}