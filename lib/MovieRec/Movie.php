<?php

namespace MovieRec;

class Movie {

    private $movieId;
    private $title;
    private $year;
    private $ratingMean;
    private $imdbId;

    /**
     * Movie constructor.
     * @param $movieId
     * @param $title
     * @param $year
     * @param $ratingMean
     * @param $imdbId
     */
    public function __construct($movieId, $title, $year, $ratingMean, $imdbId)
    {
        $this->movieId = $movieId;
        $this->title = $title;
        $this->year = $year;
        $this->ratingMean = $ratingMean;
        $this->imdbId = sprintf( '%07d', $imdbId );
    }

    /**
     * @return int
     */
    public function getMovieId() : int
    {
        return $this->movieId;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getYear() : int
    {
        return $this->year;
    }

    /**
     * @return string
     */
    public function getImdbId() : string
    {
        return $this->imdbId;
    }

    /**
     * // No return type given because of null objects to return
     * @return mixed
     */
    public function getRatingMean()
    {
        return $this->ratingMean;
    }

}
