<?php

use MovieRec\Movie;
use MovieRec\User;

require_once "vendor/autoload.php";
use Laudis\Neo4j\Databags\Statement;
use function Clue\StreamFilter\append;



class DataManager
{
    private static $__sqlConnection;
    private static $__graphConnection;

    /**
     * connect to the database
     *
     * note: alternatively put those in parameter list or as class variables
     *
     * @return connection resource
     */
    private static function getSQLConnection() : \PDO {
        if (!isset(self::$__sqlConnection)) {
            self::$__sqlConnection = new \PDO('mysql:host=movierec.c05o1lzy2gcd.eu-central-1.rds.amazonaws.com:3306;dbname=web4_movie_rec', 'root', 'x5lqrBB123');
        }
        return self::$__sqlConnection;
    }

    /**
     * @param PDOStatement $cursor
     */
    private static function close(\PDOStatement $cursor) {
        $cursor->closeCursor();
    }

    /**
     * @param PDO $connection
     * @param string $query
     * @param array $parameters
     * @return false|PDOStatement
     */
    private static function query(\PDO $connection, string $query, array $parameters = array()) {
        $statement = $connection->prepare($query);
        $i = 1;
        foreach ($parameters as $param) {
            if (is_int($param)) {
                $statement->bindValue($i, $param, \PDO::PARAM_INT);
            }
            if (is_string($param)) {
                $statement->bindValue($i, $param, \PDO::PARAM_STR);
            }
            $i++;
        }
        $statement->execute();
        return $statement;
    }

    /**
     * @param PDOStatement $cursor
     * @return mixed
     */
    private static function fetchObject(\PDOStatement $cursor) {
        return $cursor->fetchObject();
    }

    /**
     * @param string $userName
     * @return User|null
     */
    public static function getUserByUserName(string $userName) {
        $user = null;
        $con = self::getSQLConnection();
        $res = self::query($con, "
      SELECT id, username, password, graphdbid
      FROM users 
      WHERE username = ?;
      ", array($userName));
        if ($u = self::fetchObject($res)) {
            $user = new User($u->id, $u->username, $u->password, $u->graphdbid);
        }
        self::close($res);
        self::closeSqlConnection();
        return $user;
    }

    /**
     * @param int $userId
     * @return User|null
     */
    public static function getUserById(int $userId) {
        $user = null;
        $con = self::getSQLConnection();
        $res = self::query($con, "
      SELECT id, username, password, graphdbid
      FROM users 
      WHERE id = ?;
      ", array($userId));
        if ($u = self::fetchObject($res)) {
            $user = new User($u->id, $u->username, $u->password, $u->graphdbid);
        }
        self::close($res);
        self::closeSqlConnection();
        return $user;
    }

    /**
     * @param string $username
     * @param string $password
     */
    public static function insertUser(string $username, string $password) {

        $gdbid = self::generateUser();

        $con = self::getSQLConnection();
        $res = self::query($con, "
        INSERT INTO users (username, password, graphdbid)
        VALUES (?,?,?);", array($username, hash('sha1', "$username|$password"), $gdbid));
        self::close($res);
        self::closeSqlConnection();
    }


    /**
     * @return \Laudis\Neo4j\Contracts\ClientInterface
     */
    private static function getGraphConnection(): \Laudis\Neo4j\Contracts\ClientInterface
    {
        if (!isset(self::$__graphConnection)) {
            self::$__graphConnection = Laudis\Neo4j\ClientBuilder::create()
                ->addHttpConnection('backup', 'http://neo4j:x5lqrBB@ec2-3-122-100-146.eu-central-1.compute.amazonaws.com')
                ->addBoltConnection('default', 'bolt://neo4j:x5lqrBB@ec2-3-122-100-146.eu-central-1.compute.amazonaws.com')
                ->setDefaultConnection('default')
                ->build();
        }
        return self::$__graphConnection;
    }

    /**
     *
     */
    private static function closeSqlConnection() {
        self::$__sqlConnection = null;
    }

    /**
     *
     */
    private static function closeGraphConnection() {
        self::$__graphConnection = null;
    }


    /**
     * @param string $searchText
     * @return array
     */
    public static function searchMovieByName(string $searchText): array
    {
        $searchText = strtolower($searchText);

        $con = self::getGraphConnection();

        $query = new Statement('MATCH (n:Movie) WHERE toLower(n.title) CONTAINS $movieName RETURN n ORDER BY COALESCE(n.meanRating, -1) DESC LIMIT 100'
            , ['movieName' => $searchText]);

        $result = $con->runStatement($query);

        $movies = [];

        foreach ($result as $i) {
            $m =  $i->get('n');
            $meanRating = null;
            if (array_key_exists('meanRating', $m)) {
                $meanRating = $m['meanRating'];
            }

            $movie = new Movie($m['movieId'], $m['title'], $m['year'],$meanRating ,$m['imdbId']);
            array_push($movies, $movie);
        }

        self::closeGraphConnection();
        return $movies;
    }


    /**
     * @param int $movieId
     * @return Movie|null
     */
    public static function getMovieById(int $movieId) {

        $movie = null;
        $con = self::getGraphConnection();

        $query = new Statement('MATCH (m:Movie {movieId:$movieId}) RETURN m', ['movieId' => $movieId]);

        $result = $con->runStatement($query);

        foreach ($result as $i) {
            $m =  $i->get('m');

            $meanRating = null;
            if (array_key_exists('meanRating', $m)) {
                $meanRating = $m['meanRating'];
            }

            $movie = new Movie($m['movieId'], $m['title'], $m['year'], $meanRating, $m['imdbId']);
        }

        self::closeGraphConnection();
        return $movie;
    }


    /**
     * @param int $movieId
     * @return array
     */
    public static function getSimilarMovies(int $movieId): array
    {
        $query = new Statement('MATCH (m1:Movie)<-[:SIMILAR]-(m2:Movie) 
                                    WHERE m1.movieId=$movieId and not m1.movieId=m2.movieId 
                                    RETURN m2 
                                    ORDER BY m2.meanRating DESC LIMIT 10', ['movieId' => $movieId]);

        $con = self::getGraphConnection();

        $moviesReco = [];

        $result = $con->runStatement($query);

        foreach ($result as $i) {
            $m =  $i->get('m2');

            $meanRating = null;
            if (array_key_exists('meanRating', $m)) {
                $meanRating = $m['meanRating'];
            }

            $movie = new Movie($m['movieId'], $m['title'], $m['year'],$meanRating ,$m['imdbId']);
            array_push($moviesReco, $movie);
        }

        self::closeGraphConnection();
        return $moviesReco;
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getRecommendationForUser(int $userId) : array {

        $query = new Statement('MATCH (u1:User {id:$userId})-[:RATED]->(m3:Movie)
                WITH collect(distinct m3) as movies
                MATCH path = (u:User)-[:RATED]->(m1:Movie)-[s:SIMILAR]->(m2:Movie),
                (m2)-[:IN_GENRE]->(g:Genre)
                WHERE u.id=$userId and not m2 in movies
                RETURN DISTINCT m2
                ORDER BY COALESCE(m2.meanRating, -1) DESC
                LIMIT 5', ['userId' => $userId]);

        $con = self::getGraphConnection();

        $moviesReco = [];
        $result = $con->runStatement($query);

        foreach ($result as $i) {
            $m =  $i->get('m2');

            $meanRating = null;
            if (array_key_exists('meanRating', $m)) {
                $meanRating = $m['meanRating'];
            }
            $movie = new Movie($m['movieId'], $m['title'], $m['year'],$meanRating ,$m['imdbId']);
            array_push($moviesReco, $movie);
        }

        self::closeGraphConnection();
        return $moviesReco;
    }

    /**
     * @param int $userId
     * @param int $movieId
     */
    public static function addWatched(int $userId, int $movieId) {
        $query = new Statement('MATCH (u:User {id:$userId}) 
                                    MATCH (m:Movie {movieId:$movieId})
                                    MERGE (u)-[r:RATED]->(m)', ['userId' => $userId, 'movieId' => $movieId]);

        $con = self::getGraphConnection();

        $tsx = $con->openTransaction();
        $result = $tsx->runStatement($query);

        $tsx->commit();
    }


    /**
     * @param int $userId
     * @param int $movieId
     */
    public static function removeWatched(int $userId, int $movieId) {
        $query = new Statement('MATCH (u:User {id:$userId})-[r:RATED]->(m:Movie {movieId:$movieId})
                                    DELETE r', ['userId' => $userId, 'movieId' => $movieId]);

        $con = self::getGraphConnection();

        $tsx = $con->openTransaction();
        $result = $tsx->runStatement($query);

        $tsx->commit();
    }


    /**
     * @param int $userId
     * @return array
     */
    public static function getWatchedByUserId(int $userId) : array {
        $query = new Statement('MATCH (User {id:$userId})-[RATED]->(m:Movie) RETURN m ORDER BY m.meanRating DESC', ['userId' => $userId]);
        $con = self::getGraphConnection();

        $result = $con->runStatement($query);

        $movies = [];

        foreach ($result as $i) {
            $m =  $i->get('m');
            $meanRating = null;
            if (array_key_exists('meanRating', $m)) {
                $meanRating = $m['meanRating'];
            }

            $movie = new Movie($m['movieId'], $m['title'], $m['year'],$meanRating ,$m['imdbId']);
            array_push($movies, $movie);
        }
        self::closeGraphConnection();
        return $movies;
    }


    /**
     * @return int
     */
    public static function generateUser() : int {
        $queryCreate = new Statement('CREATE (u:User) RETURN ID(u)', []);

        $con = self::getGraphConnection();
        $tsx = $con->openTransaction();
        $result = $tsx->runStatement($queryCreate);

        $idCreated = $result->get(0)->get('ID(u)');

        $querySetProperty = new Statement('MATCH (u:User) WHERE ID(u)=$idCreated SET u.id = $idCreated', ['idCreated' => $idCreated]);
        $result = $tsx->runStatement($querySetProperty);

        $tsx->commit();
        return $idCreated;
    }

}