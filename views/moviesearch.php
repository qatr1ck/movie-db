<?php

use MovieRec\SimMovies;
use MovieRec\Controller;

require_once('views/partials/header.php');

$movies = DataManager::searchMovieByName($_SESSION['search']);
?>

    <div class="page-header">
        <h2 class="text-light">Movies related to '<?php echo $_SESSION['search']?>'</h2>
    </div>

    <?php require('views/partials/movielist.php'); ?>

    <?php if (SimMovies::isShown()) :
        require('views/partials/similarModal.php'); ?>
    <?php endif; ?>

<?php require_once('views/partials/footer.php');