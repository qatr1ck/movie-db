<?php require_once 'views/partials/header.php' ?>

<?php

use MovieRec\Util;

?>

    <main>
        <div class="title">
            <span>THE MOVIE RECOMMENDER</span> </br>
            <?php if (isset($user)): ?>
                <div class="subtitle">
                    <span>WELCOME <?php echo $user->getUsername() ?></span> </br>
                </div>
            <?php endif; ?>
        </div>
    </main>

<?php require_once 'views/partials/footer.php' ?>