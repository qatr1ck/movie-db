<?php
use MovieRec\AuthenticationManager;
use MovieRec\Util;
use MovieRec\Controller;

if (AuthenticationManager::isAuthenticated()) {
    Util::redirect("index.php");
}
require_once ('views/partials/header.php');
?>

    <div class="login-dark">
        <form method="post" action="<?php echo Util::action(Controller::ACTION_LOGIN, array('view' => $view)); ?>">
            <h2 class="sr-only">Please sign in</h2>

            <div class="form-floating">
                <input type="text" class="form-control" id="inputName" name="<?php echo Controller::USER_NAME; ?>">
                <label for="floatingInput">Username</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" id="inputPassword" name="<?php echo Controller::USER_PASSWORD; ?>">
                <label for="floatingPassword">Password</label>
            </div>

            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <div class="link-primary">
                <a href="index.php?view=register">Register</a>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
        </form>
    </div>


<?php require_once 'views/partials/footer.php' ?>