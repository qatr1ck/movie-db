<?php
use MovieRec\AuthenticationManager;
use MovieRec\Util;
use MovieRec\Controller;
if (AuthenticationManager::isAuthenticated()) {
    Util::redirect("index.php");
}
require_once ('views/partials/header.php');
?>
    <div class="login-dark">
        <form method="post" action="<?php echo Util::action(Controller::ACTION_REGISTER, array('view' => $view)); ?>">
            <h2 class="sr-only">New Account</h2>
            <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
            <div class="form-group"><input class="form-control" type="text" id="inputName" name="<?php echo Controller::USER_NAME; ?>" placeholder="New Username"></div>
            <div class="form-group"><input class="form-control" type="password" id="inputPassword" name="<?php echo Controller::USER_PASSWORD; ?>" placeholder="New Password"></div>
            <br/>

            <div class="form-group"><button class="btn btn-primary btn-block" type="submit">Register</button></div>
        </form>
    </div>

<?php require_once 'views/partials/footer.php' ?>