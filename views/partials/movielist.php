<?php

use MovieRec\Movie;
use MovieRec\Util;

?>

<table class="table table-hover table-dark">
    <tr>
        <th>Title</th>
        <th>Year</th>
        <th>Rating</th>
        <th>IMDB Link</th>
        <th>Show similar movies</th>
        <?php if (isset($user)): ?>
            <th>Watchlist</th>
        <?php endif; ?>
    </tr>

    <?php foreach ($movies as $m) : ?>
        <tr>
            <td> <?php echo $m->getTitle() ?> </td>
            <td>
                <?php if ($m->getYear() <= 0): ?>
                    --
                <?php else: ?>
                    <?php echo ($m->getYear()) ?>
                <?php endif; ?>
            </td>

            <td>
                <div class="rating">
                    <?php if ($m->getRatingMean() == null): ?>
                        --
                    <?php else: ?>
                        <div class="rating-upper"
                             style="width: <?php echo Util::getPercentageRating($m->getRatingMean()) ?>%">
                            <span>★★★★★</span>
                        </div>
                        <div class="rating-lower">
                            <span>★★★★★</span>
                        </div>
                    <?php endif; ?>
                </div>
            </td>
            <td><a target="_blank" href="http://imdb.com/title/tt<?php echo $m->getImdbId() ?>/"><?php echo $m->getImdbId() ?></a></td>
            <td>
                <form method="post" action="<?php echo Util::action
                (MovieRec\Controller::ACTION_SIM, array('movieId' => $m->getMovieId())); ?>">
                    <button type="submit" role="button" class="btn btn-light">
                        <i class="bi bi-arrow-bar-down" ></i>
                    </button>
                </form>
            </td>
            <?php if (isset($user)): ?>
                <td>
                    <?php if(in_array($m, $watchlist)): ?>
                        <form method="post" action="<?php echo Util::action (MovieRec\Controller::ACTION_REMOVE_WATCHED, array('movieId' => $m->getMovieId())); ?>">
                            <button type="submit" role="button" class="btn btn-outline-danger">REMOVE</button>
                        </form>
                    <?php else: ?>
                        <form method="post" action="<?php echo Util::action (MovieRec\Controller::ACTION_ADD_WATCHED, array('movieId' => $m->getMovieId())); ?>">
                            <button type="submit" role="button" class="btn btn-outline-success">ADD</button>
                        </form>
                    <?php endif; ?>
                </td>
            <?php endif; ?>

        </tr>
    <?php endforeach; ?>
</table>
