
<?php

use MovieRec\Movie;
use MovieRec\Util;
use MovieRec\SimMovies;


require_once('views/partials/header.php');
?>

<script>
    $(document).ready(function(){
        $("#myModal").modal('show');

        $('#myModal').on('hidden.bs.modal', function (e) {
            <?php SimMovies::setShown(False); ?>
        })
    });
</script>


<div id=myModal class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg bg-dark">
            <div class="modal-content bg-dark">

                <div class="page-header bg-dark">
                    <h2 class="text-light">Similar movies to <?php echo $_SESSION['simto']->getTitle() ?> </h2>
                </div>

                <?php
                if (sizeof(SimMovies::getAll()) > 0) : ?>
                    <table class="table table-hover table-dark">
                        <tr>
                            <th>Title</th>
                            <th>Year</th>
                            <th>Rating</th>
                            <th>IMDB Link</th>
                            <?php if (isset($user)): ?>
                                <th>Watchlist</th>
                            <?php endif; ?>
                        </tr>

                        <?php foreach (SimMovies::getAll() as $m) : ?>
                            <tr>
                                <td> <?php echo $m->getTitle() ?> </td>
                                <td>
                                    <?php if ($m->getYear() <= 0): ?>
                                        --
                                    <?php else: ?>
                                        <?php echo ($m->getYear()) ?>
                                    <?php endif; ?>
                                </td>

                                <td>
                                    <div class="rating">
                                        <?php if ($m->getRatingMean() == null): ?>
                                            --
                                        <?php else: ?>
                                            <div class="rating-upper"
                                                 style="width: <?php echo Util::getPercentageRating($m->getRatingMean()) ?>%">
                                                <span>★★★★★</span>
                                            </div>
                                            <div class="rating-lower">
                                                <span>★★★★★</span>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </td>
                                <td><a target="_blank" href="http://imdb.com/title/tt<?php echo $m->getImdbId() ?>/"><?php echo $m->getImdbId() ?></a></td>
                                <?php if (isset($user)): ?>
                                    <td>
                                        <?php if(in_array($m, $watchlist)): ?>
                                            <form method="post" action="<?php echo Util::action (MovieRec\Controller::ACTION_REMOVE_WATCHED, array('movieId' => $m->getMovieId())); ?>">
                                                <button type="submit" role="button" class="btn btn-outline-danger">REMOVE</button>
                                            </form>
                                        <?php else: ?>
                                            <form method="post" action="<?php echo Util::action (MovieRec\Controller::ACTION_ADD_WATCHED, array('movieId' => $m->getMovieId())); ?>">
                                                <button type="submit" role="button" class="btn btn-outline-success">ADD</button>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                <?php endif; ?>

                            </tr>
                        <?php endforeach; ?>
                    </table>
                <?php else : ?>
                        <div class="alert alert-warning" role="alert">No similar movie found!</div>
                <?php endif; ?>

            </div>
        </div>
</div>
