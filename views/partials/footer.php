<!--display error messages-->

</div> <!-- end container-fluid -->

</div> <!-- end container-bg -->

<!-- display error messages-->
<?php
use MovieRec\Util;
if (isset($errors) && is_array($errors)): ?>
    <div class="errors alert alert-danger">
        <ul>
            <?php foreach ($errors as $errMsg): ?>
                <li class="alert-light"><?php echo(Util::escape($errMsg)); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<!-- show time as footer  -->
<div class="footer">
    <hr />
    <div class="centered fixed-bottom">

        <?php if (isset($user)): ?>
            <p class="text-light"> Nr. Movies in Watchlist: <?php echo Count($watchlist) ?></p>
        <?php else: ?>
            <p class="text-light"> Login to use premium features </p>
        <?php endif; ?>
    </div>
</div> <!-- end footer -->


</body>

</html>