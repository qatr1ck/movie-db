<?php
use MovieRec\Util;
use MovieRec\Controller;
use MovieRec\AuthenticationManager;
$user = null;
$user = AuthenticationManager::getAuthenticatedUser();
if (isset($_GET["errors"])) {
    $errors = unserialize(urldecode($_GET["errors"]));
}

$watchlist = null;
if (isset($user)) {
    $watchlist = DataManager::getWatchedByUserId($user->getGraphdbId());
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title>Movie Recommender</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/bootstrap/css/bootstrap-grid.css" rel="stylesheet">
    <link href="assets/bootstrap/css/bootstrap-reboot.css" rel="stylesheet">
    <link href="assets/bootstrap/css/bootstrap-utilities.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    <script src="assets/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link rel="icon" href="assets/img/logo.svg">
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container-fluid">

        <a class="navbar-brand" href="index.php"><img src="assets/img/logo.svg" height="28" class=logo-bright alt="MovieBrand"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li  <?php if ($view === 'welcome') { ?>class="nav-item"<?php } ?>><a class="nav-link" href="index.php">Home</a></li>

                <form class="d-flex navbar-right " method="POST" action="<?php echo Util::action(Controller::ACTION_SEARCH, array('view' => $view)); ?>">
                    <input type="text" id="movieInput" name="<?php echo Controller::MOVIE_NAME; ?>" class="form-control me-2" placeholder="Search Movie" aria-label="Search" required="true" minlength="3">
                    <button class="btn btn-outline-success btn-space" type="submit">Search</button>
                    <?php if($user != null): ?>
                        <a class="btn btn-outline-success btn-space" type="submit" href="index.php?view=watchlist">Watchlist</a>
                        <a class="btn btn-outline-success btn-space" type="submit" href="index.php?view=recommendations">Recommendations</a>
                    <?php endif; ?>
                </form>
            </ul>

            <ul class="nav navbar-nav navbar-right login">
                <li class="nav-item dropdown">
                    <?php if ($user == null): ?>
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Not logged in!
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a class="dropdown-item" href="index.php?view=login">Login now</a>
                            </li>
                        </ul>
                    <?php else: ?>
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Logged in as <b><?php echo Util::escape($user->getUserName()); ?></b>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <form method="post" action="<?php echo Util::action(MovieRec\Controller::ACTION_LOGOUT);?>">
                                    <input class="btn btn-xs" role="button" type="submit" value="Logout"/>
                                </form>
                            </li>
                        </ul>
                    <?php endif; ?>
                </li>
            </ul> <!-- /. login -->
        </div>

    </div>
</nav>

<!-- Adding stars background animation -->
<div class="stars"> </div>
<div class="stars2"></div>
<div class="stars3"></div>

<div class="container-fluid">


