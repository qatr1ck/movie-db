<?php

use MovieRec\SimMovies;
use MovieRec\Util;
use MovieRec\UserRecommendations;

require_once('views/partials/header.php');

?>

<?php if (isset($user)): ?>

    <div class="page-header">
        <h2 class="text-light">You may also like:</h2>
    </div>

    <?php if (isset($watchlist)) : ?>
        <?php
        if (sizeof($watchlist) > 0) :
            $movies = UserRecommendations::getAll();
            require('views/partials/movielist.php'); ?>

            <?php if (SimMovies::isShown()) :
                require('views/partials/similarModal.php'); ?>
            <?php endif; ?>

        <?php else :?>
            <div class="alert alert-warning" role="alert">Add some movies to your watchlist first.</div>
        <?php endif; ?>

    <?php else : ?>
        <div class="alert alert-info" role="alert">Watchlist not found!</div>
    <?php endif; ?>

<?php endif; ?>


<?php require_once('views/partials/footer.php');