<?php


class Debug
{
    public static function log(string $file, $data) : bool {
        $myfile = fopen($file, "w");
        fwrite($myfile, $data);
        fclose($myfile);
        return true;
    }
}